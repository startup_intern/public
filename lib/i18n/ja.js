/*eslint-disable max-len,quotes*/
export default {
  "about.librariesIntro": "このサイトはデモサイトです",
  "about.specialThanks": "このリンク {link} のおかげで色々なものを作れています {redux}!",
  "account.home.intro": "ひとまずいろいろ試してみよう。",
  "account.home.link.superSecretArea": "ここは秘密のページです",
  "account.home.steps": "このリンク {logoutLink} またはこのリンク {secretAreaLink} を",
  "explore.legend": "テスト 'Go':",
  "header.builtWith": "StartupIntern.com",
  "footer.builtWith": "このリンクを押してください {link}",
  "forbidden": "Verboten",
  "forbiddenReason": "このサイトにアクセスできません。",
  "home.intro": "このサイトはredux {linkRedux} とrouter {linkRouter} がパートナーです",
  "home.intro2": "このサイトは開発用に作ったものです",
  "home.intro3": "テスト表示 {linkIssues}, テスト表示です。よ",
  "home.intro3.dropAnIssue": "問題を上げてください",
  "home.welcome": "ようこそ",
  "stargazers.subtitle": "サブタイトル",
  "superSecretArea.info": "こちらは情報になります。",
  "register.startup": "スタートアップ会社として登録",
  "register.student": "学生、インターンとして登録",
  "register.company": "会社として登録"
}
