import * as constants from '../constants'
import apiClient from '../utils/api'

export function register (form, redirect) {
  console.log(form)
  apiClient().post('/user', {
    email: form.email,
    password: form.password,
    password_confirmation: form.passwordConfirmation,
    firstName: form.firstName,
    lastName: form.lastName,
    photo: form.photo,
    categoryId: form.categoryId,
    countryId: form.countryId,
    university: form.university,
    universityAddress: form.universityAddress,
    resume: form.resume,
    role: form.roleTags,
    language: form.languageTags,
    location: form.location,
    video: form.video,
    facebook: form.facebook,
    linkedin: form.linkedin,
    github: form.github,
    skypeId: form.skypeId,
    authId: 1,
    facebookId: form.facebookId,
    facebookToken: form.facebookToken
  })
      .then((response) => {
        console.log(response)
      })
      .catch((response) => {
        console.log(response)
      } )

  return setTimeout(() => {
    // Can be used to navigate to a new route
    if (redirect) {
      redirect()
    }
  }, 300)
}

export function login (state, history, data) {
  let nextPath = '/'

  return (dispatch, getState) => {
    setTimeout(() => {
      let state = getState()
      if (Object.keys(data).length !== 0) {
        if (data.token) {
          const token = Math.random().toString(36).substring(7)
          dispatch({
            type: constants.LOGGED_IN,
            payload: { token },
            user: {
              firstName: data.profile.first_name,
              lastName: data.profile.last_name,
              email: data.user.email,
              photo: data.profile.photo,
              authId: data.user.auth_id
            }
          })
          // Can be used to navigate to a new route
          history.pushState({}, nextPath)
        } else {
          dispatch({
            type: constants.LOGGED_IN_ERROR,
            payload: {
              message: data.msg,
              errorNo: data.error_no
            }
          })
          nextPath = state.router.location.pathname
          history.pushState({ data }, nextPath)
        }
      } else {
        dispatch({
          type: constants.LOGGED_IN_ERROR,
          payload: {
            message: 'time out',
            statusCode: 408
          }
        })
        nextPath = state.router.location.pathname
        history.pushState({}, nextPath)
      }
    }, 2000)
    // simulate request
  }
}

export function accept (userToken, redirect) {
  let data = {}
  apiClient().put('/user/accept/' + userToken)
      .then((response) => {
        console.log(response)
        if (response.data.data.token)
        {
          localStorage.setItem('Authorization', response.data.data.token)
          data = response.data
        }
      } )
      .catch((response) => {
        console.log(response)
      } )

  return dispatch => {
    const token = Math.random().toString(36).substring(7)
    // simulate request
    setTimeout(() => {
      dispatch({
        type: constants.LOGGED_IN,
        payload: { token },
        user: {
          firstName: data.profile.first_name,
          lastName: data.profile.last_name,
          email: data.user.email,
          photo: data.profile.photo,
          authId: data.user.auth_id
        }
      })
      // Can be used to navigate to a new route
      if (redirect) {
        redirect()
      }
    }, 300)
  }

}

export function addFaceBookData (token, facebookResponse) {

  return { type: constants.ADD_FACEBOOK_DATA,
      user: {
        facebookId: facebookResponse.id,
        facebookToken: token,
        firstName: facebookResponse.first_name,
        lastName: facebookResponse.last_name,
        email: facebookResponse.email,
        photo: facebookResponse.picture.data.url
      }
    }
}
export function switchLocale (locale) {
  return { type: constants.LOCALE_SWITCHED, payload: locale }
}

export function hideError () {
  return { type: constants.HIDE_ERROR }
}
