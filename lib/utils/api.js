import axios from 'axios'

const API_URL = process.env.API_URL

const apiClient = (authorization = null) => {
  return axios.create ( {
    baseURL: API_URL,
    timeout: 5000,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers':
          'Origin, X-Requested-With, Content-Type, Accept',
      'xhrFields':{
        'withCredentials': 'true'
      },
      'Authorization': 'Bearer ' + authorization
    }
  })
}

export default apiClient
