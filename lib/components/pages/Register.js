import React, { PropTypes } from 'react'
import { defineMessages, FormattedMessage } from 'react-intl'

const messages = defineMessages({
  startup: {
    id: 'register.startup',
    description: 'startup button',
    defaultMessage: 'Register as startup company'
  },
  student: {
    id: 'register.student',
    description: 'student button',
    defaultMessage: 'Register as student or trainer'
  },
  company: {
    id: 'register.company',
    description: 'company button',
    defaultMessage: 'Register as company'
  }
})

export default class Register extends React.Component {

  static propTypes = {
    location: PropTypes.object
  };

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  };


  render () {
    const linkRegisterIntern = (
    <a href="/register/intern" className="pure-button pure-button-primary">
      <FormattedMessage {...messages.student} />
    </a>)
    const linkRegisterStartup = (
    <a href="/register/startup" className="pure-button pure-button-primary">
      <FormattedMessage {...messages.startup} />
    </a>)
    const linkRegisterCompany = (
    <a href="/register/company" className="pure-button pure-button-primary">
      <FormattedMessage {...messages.company} />
    </a>)
    return (
      <div>
        <div className="header">
          <h1>Register</h1>
        </div>
        <div className="content row">
          <div className="pure-control-group col-md-2 col-md-offset-5">
            {linkRegisterIntern}
          </div><br />
          <div className="pure-control-group col-md-2 col-md-offset-5">
            {linkRegisterStartup}
          </div><br />
          <div className="pure-control-group col-md-2 col-md-offset-5">
            {linkRegisterCompany}
          </div>
        </div>
      </div>
    )
  }
}
