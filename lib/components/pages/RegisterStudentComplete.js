import React, { PropTypes } from 'react'
import { defineMessages, FormattedMessage } from 'react-intl'

const messages = defineMessages({
  registerMessage: {
    id: 'registerComplete.registerMessage',
    description: 'registerMessage',
    defaultMessage: 'Complete temporary register. please check email and complete register account.'
  }
})

export default class RegisterStudentComplete extends React.Component {

  static propTypes = {
    location: PropTypes.object
  }

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className="text-left">
        <div className="pure-control-group">
          <span><FormattedMessage {...messages.registerMessage} /></span>
        </div>
      </div>
    )
  }
}


RegisterStudentComplete.propTypes = {
  location: React.PropTypes.object,
  history: React.PropTypes.object
}
