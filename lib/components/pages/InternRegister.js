import React, { PropTypes } from 'react'
import * as actions from '../../actions/application'

export default class InternRegister extends React.Component {

  static propTypes = {
    location: PropTypes.object
  };

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  };

  constructor (props) {
    super(props)
    this.state = {
      email: null,
      password: null,
      firstName: null,
      lastName: null,
      country: null,
      university: null,
      universityAddress: null,
      role: null,
      language: null,
      location: null,
      video: null,
      facebook: null,
      linkedin: null,
      github: null,
      skypeid: null,
    }
  }

  handleInputChange (evt) {
    this.setState({
      [evt.target.name]: evt.target.value
    })
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const { history, store } = this.context
    const { location } = this.props

    let nextPath = '/account'
    if (location.state && location.state.nextPathname) {
      nextPath = location.state.nextPathname
    }

    store.dispatch(actions.login(this.state, () => {
      // redirect to a secure page
      history.pushState({}, nextPath)
    }))
  }

  render () {
    return (
      <div>
        <div className="header">
          <h1>Register</h1>
        </div>
        <div className="content">
          <form
            className="explore pure-form pure-form-aligned"
            onSubmit={::this.handleSubmit}
            onChange={::this.handleInputChange}>
            <fieldset>
              <div className="pure-control-group">
                <label htmlFor="email">Email</label>
                <input type="email" name="email" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="password">Password</label>
                <input type="password" name="password" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="firstName">FirstName</label>
                <input type="text" name="firstName" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="lastName">LastName</label>
                <input type="text" name="lastName" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="country">Country</label>
                <input type="text" name="country" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="university">University</label>
                <input type="text" name="university" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="universityAddress">UniversityAddress</label>
                <input type="text" name="universityAddress" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="resume">Resume</label>
                <input type="text" name="resume" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="role">Role</label>
                <select name="role" defaultValue="" >
                  <option value="php">php</option>
                  <option value="html">html</option>
                  <option value="css">css</option>
                  <option value="ruby">ruby</option>
                </select>
              </div>
              <div className="pure-control-group">
                <label htmlFor="language">Language</label>
                <select name="language" defaultValue="" >
                  <option value="english">english</option>
                  <option value="japanese">japanese</option>
                  <option value="korean">korean</option>
                  <option value="german">german</option>
                </select>
              </div>
              <div className="pure-control-group">
                <label htmlFor="location">CompanyLocation</label>
                <input type="text" name="location" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="password">Password</label>
                <input type="text" name="password" defaultValue="secret" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="video">Video</label>
                <input type="text" name="video" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="facebook">Facebook</label>
                <input type="text" name="facebook" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="linkedin">LinkedIn</label>
                <input type="text" name="linkedin" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="github">GitHub</label>
                <input type="text" name="github" defaultValue="" />
              </div>
              <div className="pure-control-group">
                <label htmlFor="skypeid">SkypeId</label>
                <input type="text" name="skypeid" defaultValue="" />
              </div>
              <button type="submit"
                className="pure-button pure-button-primary"
                >Confirm</button>
            </fieldset>
          </form>
        </div>
      </div>
    )
  }
}
