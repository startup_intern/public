import React, { PropTypes } from 'react'
import { defineMessages, FormattedMessage } from 'react-intl'
import { ButtonToolbar } from 'react-bootstrap'
import apiClient from '../../utils/api'
import { WithContext as ReactTags } from 'react-tag-input'

const messages = defineMessages({
  email: {
    id: 'registerStudent.email',
    description: 'email',
    defaultMessage: 'Email'
  },
  password: {
    id: 'registerStudent.password',
    description: 'password',
    defaultMessage: 'Password'
  },
  passwordConfirmation: {
    id: 'registerStudent.passwordConfirmation',
    description: 'passwordConfirmation',
    defaultMessage: 'password confirmation'
  },
  firstName: {
    id: 'registerStudent.firstName',
    description: 'firstName',
    defaultMessage: 'FirstName'
  },
  lastName: {
    id: 'registerStudent.lastName',
    description: 'lastName',
    defaultMessage: 'LastName'
  },
  photo: {
    id: 'registerStudent.lastName',
    description: 'photo',
    defaultMessage: 'Photo'
  },
  category: {
    id: 'registerStudent.category',
    description: 'category',
    defaultMessage: 'Category'
  },
  country: {
    id: 'registerStudent.country',
    description: 'country',
    defaultMessage: 'Country'
  },
  universityName: {
    id: 'registerStudent.universityName',
    description: 'universityName',
    defaultMessage: 'University'
  },
  universityAddress: {
    id: 'registerStudent.universityAddress',
    description: 'universityAddress',
    defaultMessage: 'UniversityAddress'
  },
  resume: {
    id: 'registerStudent.resume',
    description: 'resume',
    defaultMessage: 'Resume'
  },
  role: {
    id: 'registerStudent.role',
    description: 'role',
    defaultMessage: 'Role'
  },
  language: {
    id: 'registerStudent.language',
    description: 'language',
    defaultMessage: 'Language'
  },
  location: {
    id: 'registerStudent.location',
    description: 'location',
    defaultMessage: 'WorkLocation'
  },
  video: {
    id: 'registerStudent.video',
    description: 'introduceVideo',
    defaultMessage: 'Video'
  },
  facebook: {
    id: 'registerStudent.facebook',
    description: 'facebook',
    defaultMessage: 'Facebook'
  },
  linkedin: {
    id: 'registerStudent.linkedin',
    description: 'linkedin',
    defaultMessage: 'LinkedIn'
  },
  github: {
    id: 'registerStudent.github',
    description: 'github',
    defaultMessage: 'GitHub'
  },
  skypeid: {
    id: 'registerStudent.skypeid',
    description: 'skypeid',
    defaultMessage: 'SkypeId'
  },
  add: {
    id: 'registerStudent.add',
    description: 'add',
    defaultMessage: 'Add'
  },
  back: {
    id: 'registerStudent.back',
    description: 'back',
    defaultMessage: 'Back'
  },
  confirm: {
    id: 'registerStudent.confirm',
    description: 'confirm',
    defaultMessage: 'Confirm'
  },
  select: {
    id: 'select.confirm',
    description: 'select',
    defaultMessage: 'Please select'
  },
  emptyError: {
    id: 'emptyError.confirm',
    description: 'empty error',
    defaultMessage: 'Please input this column'
  },
  emailAddressError: {
    id: 'emailError.confirm',
    description: 'email error',
    defaultMessage: 'Please input email format'
  },
  emailAlreadyRegisterError: {
    id: 'emailError.confirm',
    description: 'already registered error',
    defaultMessage: 'this email already registered'
  },
  passwordMatchError: {
    id: 'passwordMatchError.confirm',
    description: 'password match error',
    defaultMessage: 'password doesn\'t match'
  },
  shortCharError: {
    id: 'shortCharError.confirm',
    description: 'short character error',
    defaultMessage: 'Please input more 8 character'
  },
  photoError: {
    id: 'photoError.confirm',
    description: 'photo error',
    defaultMessage: 'Please choose image file'
  },
})

export default class RegisterStudent extends React.Component {

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  }

  constructor (props, context) {
    super(props)

    let stores = context.store.getState()
    let email = ''
    let firstName = ''
    let lastName = ''
    let facebookId = ''
    let facebookToken = ''
    let photo = ''
    if (stores.user.user.facebookToken && typeof props.location.state !== 'undefined') {
      email = stores.user.user.email
      firstName = stores.user.user.firstName
      lastName = stores.user.user.lastName
      facebookId = stores.user.user.facebookId
      facebookToken = stores.user.user.facebookToken
      photo = stores.user.user.photo
    } else {
      email = props.location.state.email
      firstName = props.location.state.firstName
      lastName = props.location.state.lastName
      facebookId = props.location.state.facebookId
      facebookToken = props.location.state.facebookToken
      photo = props.location.state.photo
    }

    this.state = {
      email: email,
      password: props.location.state.password,
      passwordConfirmation: props.location.state.passwordConfirmation,
      firstName: firstName,
      lastName: lastName,
      categoryId: props.location.state.categoryId,
      countryId: props.location.state.countryId,
      universityName: props.location.state.universityName,
      universityAddress: props.location.state.universityAddress,
      photo: photo,
      resume: props.location.state.resume,
      role: props.location.state.role,
      language: props.location.state.language,
      location: props.location.state.location,
      video: props.location.state.video,
      facebook: props.location.state.facebook,
      linkedin: props.location.state.linkedin,
      github: props.location.state.github,
      skypeId: props.location.state.skypeId,
      facebookId: facebookId,
      facebookToken: facebookToken,
      languageTags: [],
      languageSuggestions: [],
      languageList: [],
      roleTags: [],
      roleSuggestions: [],
      roleList: [],
      categoryList: [],
      countryList: [],
      errors: {}
    }
    this.roleDelete = this.roleDelete.bind(this)
    this.roleAddition = this.roleAddition.bind(this)
    this.roleDrag = this.roleDrag.bind(this)
    this.languageDelete = this.languageDelete.bind(this)
    this.languageAddition = this.languageAddition.bind(this)
    this.languageDrag = this.languageDrag.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.uploadPhoto = this.uploadPhoto.bind(this)
  }

  roleDelete (i) {
    let tags = this.state.roleTags
    tags.splice (i, 1)
    this.setState ( {
      roleTags: tags
    } )
  }

  userRegisterCheck (email) {
    apiClient().post('/user/register/check', {
      email: email
    })
        .then((response) => {
          console.log(response)
          if (!response.data.user) {
            return false
          } else {
            return true
          }
        } )
        .catch((response) => {
          console.log(response)
          return false
        } )
  }

  roleAddition (tag) {
    let BreakException = {}
    let tags = []
    try {
      if (this.state.roleTags) {
        tags = this.state.roleTags
      }
      this.state.roleList.forEach ((role) => {
        if (role.name === tag) {
          tags.push({
            id: role.id,
            text: tag
          })
          this.setState(
            {
              roleTags: tags
            }
          )
          throw BreakException
        }
      } )
      apiClient().post('/role/', {
        name: tag
      })
          .then((response) => {
            if (!response.data.error) {
              tags.push({
                id: response.data.data.id,
                text: response.data.data.name
              })
              this.setState(
                {
                  roleTags: tags
                }
              )
            }
          } )
          .catch((response) => {
            console.log(response)
          } )
    } catch (e) {
      if (e!==BreakException) {
        throw e
      }
    }
  }

  roleDrag (tag, currPos, newPos) {
    let tags = this.state.roleTags

    // mutate array
    tags.splice(currPos, 1)
    tags.splice(newPos, 0, tag)

    // re-render
    this.setState(
      {
        roleTags: tags
      }
    )
  }

  languageDelete (i) {
    let tags = this.state.languageTags
    tags.splice (i, 1)
    this.setState ( {
      languageTags: tags
    } )
  }

  languageAddition (tag) {
    let BreakException = {}
    let tags = []
    try {
      if (this.state.languageTags) {
        tags = this.state.languageTags
      }
      this.state.languageList.forEach ((language) => {
        if (language.name === tag) {
          tags.push({
            id: language.id,
            text: tag
          })
          this.setState(
            {
              languageTags: tags
            }
          )
          throw BreakException
        }
      } )
    } catch (e) {
      if (e!==BreakException) {
        throw e
      }
    }
  }

  languageDrag (tag, currPos, newPos) {
    let tags = this.state.languageTags

    // mutate array
    tags.splice(currPos, 1)
    tags.splice(newPos, 0, tag)

    // re-render
    this.setState(
      {
        languageTags: tags
      }
    )
  }

  handleInputChange (evt) {
    this.setState({
      [evt.target.name]: evt.target.value
    })
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const { history } = this.context

    let errors = {}
    let nextPath = '/register/student/confirm'
    let pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (!this.state.email) {
      errors.email = ( <FormattedMessage {...messages.emptyError} /> )
    } else if (!this.state.email.match(pattern)) {
      errors.email = ( <FormattedMessage {...messages.emailAddressError} /> )
    } else if (this.userRegisterCheck(this.state.email)) {
      errors.email = ( <FormattedMessage {...messages.emailAlreadyRegisterError} /> )
    }
    if (!this.state.password) {
      errors.password = ( <FormattedMessage {...messages.emptyError} /> )
    } else if (this.state.password !== this.state.passwordConfirmation) {
      errors.password = ( <FormattedMessage {...messages.passwordMatchError} /> )
    }

    if (!this.state.firstName) {
      errors.firstName = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (!this.state.lastName) {
      errors.lastName = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (!this.state.photo) {
      errors.photo = ( <FormattedMessage {...messages.photoError} /> )
    }
    if (!this.state.countryId) {
      errors.countryId = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (!this.state.resume) {
      errors.resume = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (!this.state.location) {
      errors.lastName = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (!this.state.video) {
      errors.video = ( <FormattedMessage {...messages.emptyError} /> )
    }

    if (!this.state.skypeId) {
      errors.skypeId = ( <FormattedMessage {...messages.emptyError} /> )
    }
    if (typeof errors.length !== 'undefined') {
      this.setState({
        errors: errors
      })
    } else {
      history.pushState(this.state, nextPath)
    }
  }

  uploadPhoto (evt) {
    let imageFormData = new FormData()
    imageFormData.append('photo', evt.target.files[0])
    apiClient().post('/upload/image', imageFormData)
        .then((response) => {
          if (!response.data.error) {
            this.setState(
              {
                photo: response.data.data.path + '/' + response.data.data.fileName
              }
            )
          }
        } )
        .catch((response) => {
          console.log(response)
        } )
  }

  componentWillMount () {
    let languages = []
    let roles = []

    const { store } = this.context
    let state = store.getState()
    if (state.user.user.email) {

    }

    apiClient().get('/language/')
        .then((response) => {
          response.data.data.forEach( (languageData) => {
            languages.push(languageData.name)
          })
          this.setState( {
            languageSuggestions: languages,
            languageList: response.data.data
          } )
          let tags = this.props.location.state.languageTags
          this.setState(
            {
              languageTags: tags
            }
          )
        } )
        .catch((response) => {
          console.log(response)
        } )
    apiClient().get('/role/')
        .then((response) => {
          response.data.data.forEach( (roleData) => {
            roles.push(roleData.name)
          })
          this.setState( {
            roleSuggestions: roles,
            roleList: response.data.data
          } )
          let tags = this.props.location.state.roleTags
          this.setState(
            {
              roleTags: tags
            }
          )
        } )
        .catch((response) => {
          console.log(response)
        } )

    apiClient().get('/category/')
        .then((response) => {
          this.setState( {
            categoryList: response.data.data
          } )
        } )
        .catch((response) => {
          console.log(response)
        } )

    apiClient().get('/country/')
        .then((response) => {
          this.setState( {
            countryList: response.data.data
          } )
        } )
        .catch((response) => {
          console.log(response)
        } )
  }

  render () {
    let roleTags = this.state.roleTags
    let roleSuggestions = this.state.roleSuggestions
    let languageTags = this.state.languageTags
    let languageSuggestions = this.state.languageSuggestions
    let countryList = this.state.countryList
    let categoryList = this.state.categoryList

    return (
      <div className="text-left">
        <form className="explore pure-form pure-form-aligned"
              onSubmit={::this.handleSubmit}
              onChange={::this.handleInputChange}>
          <fieldset>
            <div className="pure-control-group">
              <label htmlFor="email">
                <span className="text-danger">*</span><FormattedMessage {...messages.email} />
              </label>
              <span className="text-danger">{this.state.errors.email}</span>
              <input type="text" name="email" value={this.state.email} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="password">
                <span className="text-danger">*</span><FormattedMessage {...messages.password} />
              </label>
              <span className="text-danger">{this.state.errors.password}</span>
              <input type="password" name="password" value={this.state.password} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="passwordConfirmation">
                <span className="text-danger">*</span><FormattedMessage {...messages.passwordConfirmation} />
              </label>
              <span className="text-danger">{this.state.errors.passwordConfirmation}</span>
              <input type="password" name="passwordConfirmation" value={this.state.passwordConfirmation} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="firstName">
                <span className="text-danger">*</span><FormattedMessage {...messages.firstName} />
              </label>
              <span className="text-danger">{this.state.errors.firstName}</span>
              <input type="text" name="firstName" value={this.state.firstName} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="lastName">
                <span className="text-danger">*</span><FormattedMessage {...messages.lastName} />
              </label>
              <span className="text-danger">{this.state.errors.lastName}</span>
              <input type="text" name="lastName" value={this.state.lastName} />
            </div>
            <div>
              <label htmlFor="photo">
                <FormattedMessage {...messages.photo} />
              </label>
              <span className="text-danger">{this.state.errors.photo}</span>
              <label className="btn btn-default btn-file">
                { (() => {
                  if (this.state.photo) {
                    return (<img src= { this.state.photo } style= { { height: '200px' } } />)
                  }
                } )() }
                <input type="file" className="text-center" name="photo" onChange={::this.uploadPhoto} />
              </label>
            </div>
            <div className="pure-control-group">
              <label htmlFor="category_id">
                <span className="text-danger">*</span><FormattedMessage {...messages.category} />
              </label>
              <span className="text-danger">{this.state.errors.categoryId}</span>
              <select name="categoryId" >
                <FormattedMessage {...messages.select}>
                  {(select) => <option value="">{select}</option>}
                </FormattedMessage>
                {categoryList.map( (category) => {
                  return (
                      <option value={category.id}>{category.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="pure-control-group">
              <label htmlFor="country_id">
                <span className="text-danger">*</span><FormattedMessage {...messages.country} />
              </label>
              <span className="text-danger">{this.state.errors.countryId}</span>
              <select name="countryId" >
                <FormattedMessage {...messages.select}>
                  {(select) => <option value="">{select}</option>}
                </FormattedMessage>
              {countryList.map( (country) => {
                return (
                    <option value={country.id}>{country.name}</option>
                )
              })}
              </select>
            </div>
            <div className="pure-control-group">
              <label htmlFor="university">
                <FormattedMessage {...messages.universityName} />
              </label>
              <input type="text" name="universityName" value={this.state.universityName} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="universityAddress">
                <FormattedMessage {...messages.universityAddress} />
              </label>
              <input type="text" name="universityAddress" value={this.state.universityAddress} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="resume">
                <span className="text-danger">*</span><FormattedMessage {...messages.resume} />
              </label>
              <span className="text-danger">{this.state.errors.resume}</span>
              <textarea name="resume" value={this.state.resume} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="role">
                <FormattedMessage {...messages.role} />
              </label>
              <ReactTags tags={roleTags}
                         suggestions={roleSuggestions}
                         handleDelete={this.roleDelete}
                         handleAddition={this.roleAddition}
                         handleDrag={this.roleDrag} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="language">
                <FormattedMessage {...messages.language} />
              </label>
              <ReactTags tags={languageTags}
                         suggestions={languageSuggestions}
                         handleDelete={this.languageDelete}
                         handleAddition={this.languageAddition}
                         handleDrag={this.languageDrag} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="location">
                <span className="text-danger">*</span><FormattedMessage {...messages.location} />
              </label>
              <span className="text-danger">{this.state.errors.location}</span>
              <input type="text" name="location" value={this.state.location} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="video">
                <span className="text-danger">*</span><FormattedMessage {...messages.video} />
              </label>
              <span className="text-danger">{this.state.errors.video}</span>
              <input type="text" name="video" value={this.state.video} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="facebook">Facebook</label>
              <input type="text" name="facebook" value={this.state.facebook} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="linkedin">LinkedIn</label>
              <input type="text" name="linkedin" value={this.state.linkedin} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="github">GitHub</label>
              <input type="text" name="github" value={this.state.github} />
            </div>
            <div className="pure-control-group">
              <label htmlFor="skypeId">
                <span className="text-danger">*</span>
                SkypeId
              </label>
              <span className="text-danger">{this.state.errors.skypeId}</span>
              <input type="text" name="skypeId" value={this.state.skypeId} />
            </div>

          <ButtonToolbar>
            <button bsStyle="primary">
              <FormattedMessage {...messages.back} />
            </button>
            <button bsStyle="primary">
              <FormattedMessage {...messages.confirm} />
            </button>
          </ButtonToolbar>
          </fieldset>
        </form>
      </div>
    )
  }
}

RegisterStudent.propTypes = {
  loginModalIsOpen: React.PropTypes.bool,
  registerSocialModalIsOpen: React.PropTypes.bool,
  registerModalIsOpen: React.PropTypes.bool,
  registerNextPass: React.PropTypes.string,
  email: React.PropTypes.string,
  password: React.PropTypes.string,
  passwordConfirmation: React.PropTypes.string,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  photo: React.PropTypes.string,
  country: React.PropTypes.string,
  universityName: React.PropTypes.string,
  universityAddress: React.PropTypes.string,
  resume: React.PropTypes.string,
  role: React.PropTypes.array,
  language: React.PropTypes.array,
  location: React.PropTypes.string,
  video: React.PropTypes.string,
  facebook: React.PropTypes.string,
  linkedin: React.PropTypes.string,
  github: React.PropTypes.string,
  skypeId: React.PropTypes.string,
  facebookId: React.PropTypes.string,
  facebookToken: React.PropTypes.string,
  languageTags: React.PropTypes.array,
  languageSuggestions: React.PropTypes.array,
  languageList: React.PropTypes.array,
  roleTags: React.PropTypes.array,
  roleSuggestions: React.PropTypes.array,
  roleList: React.PropTypes.array,
  categories: React.PropTypes.array,
  categoryList: React.PropTypes.array,
  countries: React.PropTypes.array,
  countryList: React.PropTypes.array,
  errors: React.PropTypes.object,
  state: React.PropTypes.array
}
