import React, { PropTypes } from 'react'
import { defineMessages, FormattedMessage } from 'react-intl'
import * as actions from '../../actions/user'

const messages = defineMessages({
  acceptMessage: {
    id: 'registerStudent.acceptMessage',
    description: 'acceptMessage',
    defaultMessage: 'complete your account. you can login by your account'
  },
  topLink: {
    id: 'registerStudent.email',
    description: 'email',
    defaultMessage: 'Email'
  },
  tokenMissMatchError: {
    id: 'shortCharError.tokenMissMatchError',
    description: 'token miss match error',
    defaultMessage: 'Can\'t accept user. please register user again.'
  },
})

export default class RegisterAccept extends React.Component {

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.state = {
      user_token: null,
      errors: {}
    }

  }

  componentWillMount () {
    const { history, store } = this.context
    let nextPath = '/'
    store.dispatch(actions.accept(this.props.params.user_token, () => {
      // redirect to a secure page
      history.pushState({}, nextPath)
    }))
  }

  render () {

    return (
      <div className="text-left">
        <div className="pure-control-group">
          <FormattedMessage {...messages.acceptMessage} />
        </div>
      </div>
    )
  }
}

RegisterAccept.propTypes = {
  loginModalIsOpen: React.PropTypes.string,
  user_token: React.PropTypes.string,
  params: React.PropTypes.object
}
