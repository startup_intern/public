/*eslint-disable max-len*/
import React from 'react'
import { connect } from 'react-redux'
import HomeStudent from '../student/HomeStudent'
import HomeGuest from '../guest/HomeGuest'

export default class Home extends React.Component {

  constructor (props) {
    super(props)
  }

  render () {
    let homeContent = <HomeGuest {...this}/>
    if (this.props.user.user.authId) {
      if (this.props.user.user.authId === 1) {
        homeContent = <HomeStudent {...this}/>
      } else if (this.props.user.authId === 2) {
        homeContent = <HomeStudent {...this}/>
      } else if (this.props.user.authId === 3) {
        homeContent = <HomeStudent {...this}/>
      }
    }
    return (
      <div>
        {homeContent}
      </div>
    )
  }
}
Home.propTypes = {
  user: React.PropTypes.shape({
    user: React.PropTypes.shape({
      email: React.PropTypes.string,
      firstName: React.PropTypes.string,
      lastName: React.PropTypes.string,
      photo: React.PropTypes.string,
      facebookId: React.PropTypes.string,
      facebookToken: React.PropTypes.string,
      authId: React.PropTypes.integer
    }),
    authId: React.PropTypes.integer
  })
}

function mapStateToProps (state) {
  return {
    user: {
      user: {
        email: state.user.user.email,
        firstName: state.user.user.firstName,
        lastName: state.user.user.lastName,
        photo: state.user.user.photo,
        facebookId: state.user.user.facebookId,
        facebookToken: state.user.user.facebookToken,
        authId: state.user.user.authId
      }
    }
  }
}

export default connect(
    mapStateToProps
)(Home)

