import React, { PropTypes } from 'react'
import { defineMessages, FormattedMessage } from 'react-intl'
import { ButtonToolbar } from 'react-bootstrap'
import * as actions from '../../actions/user'

const messages = defineMessages({
  email: {
    id: 'registerStudent.email',
    description: 'email',
    defaultMessage: 'Email'
  },
  password: {
    id: 'registerStudent.password',
    description: 'password',
    defaultMessage: 'Password'
  },
  passwordConfirmation: {
    id: 'registerStudent.passwordConfirmation',
    description: 'passwordConfirmation',
    defaultMessage: 'password confirmation'
  },
  firstName: {
    id: 'registerStudent.firstName',
    description: 'firstName',
    defaultMessage: 'FirstName'
  },
  lastName: {
    id: 'registerStudent.lastName',
    description: 'lastName',
    defaultMessage: 'LastName'
  },
  photo: {
    id: 'registerStudent.lastName',
    description: 'photo',
    defaultMessage: 'Photo'
  },
  category: {
    id: 'registerStudent.category',
    description: 'category',
    defaultMessage: 'Category'
  },
  country: {
    id: 'registerStudent.country',
    description: 'country',
    defaultMessage: 'Country'
  },
  universityName: {
    id: 'registerStudent.universityName',
    description: 'universityName',
    defaultMessage: 'University'
  },
  universityAddress: {
    id: 'registerStudent.universityAddress',
    description: 'universityAddress',
    defaultMessage: 'UniversityAddress'
  },
  resume: {
    id: 'registerStudent.resume',
    description: 'resume',
    defaultMessage: 'Resume'
  },
  role: {
    id: 'registerStudent.role',
    description: 'role',
    defaultMessage: 'Role'
  },
  language: {
    id: 'registerStudent.language',
    description: 'language',
    defaultMessage: 'Language'
  },
  location: {
    id: 'registerStudent.location',
    description: 'location',
    defaultMessage: 'WorkLocation'
  },
  video: {
    id: 'registerStudent.video',
    description: 'introduceVideo',
    defaultMessage: 'Video'
  },
  facebook: {
    id: 'registerStudent.facebook',
    description: 'facebook',
    defaultMessage: 'Facebook'
  },
  linkedin: {
    id: 'registerStudent.linkedin',
    description: 'linkedin',
    defaultMessage: 'LinkedIn'
  },
  github: {
    id: 'registerStudent.github',
    description: 'github',
    defaultMessage: 'GitHub'
  },
  skypeid: {
    id: 'registerStudent.skypeid',
    description: 'skypeid',
    defaultMessage: 'SkypeId'
  },
  add: {
    id: 'registerStudent.add',
    description: 'add',
    defaultMessage: 'Add'
  },
  back: {
    id: 'registerStudent.back',
    description: 'back',
    defaultMessage: 'Back'
  },
  register: {
    id: 'registerStudent.confirm',
    description: 'confirm',
    defaultMessage: 'Register'
  }
})

export default class RegisterStudentConfirm extends React.Component {

  static propTypes = {
    location: PropTypes.object
  }

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.state = {
      email: null,
      password: null,
      passwordConfirmation: null,
      firstName: null,
      lastName: null,
      photo: null,
      countryId: null,
      universityName: null,
      universityAddress: null,
      resume: null,
      role: null,
      language: null,
      location: null,
      video: null,
      facebook: null,
      linkedin: null,
      github: null,
      skypeId: null,
      facebookId: null,
      facebookToken: null,
      languageTags: [],
      languageSuggestions: [],
      languageList: [],
      roleTags: [],
      roleSuggestions: [],
      roleList: [],
      errors: {}
    }

  }
  backSubmit (evt) {
    evt.preventDefault()
    let nextPath = '/register/student'
    this.props.history.pushState(this.props.location.state, nextPath)
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const { history, store } = this.context
    const { location } = this.props
    let nextPath = '/register/student/complete'
    if (location.state && location.state.nextPathname) {
      nextPath = location.state.nextPathname
    }

    store.dispatch(actions.register(this.props.location.state, () => {
      // redirect to a secure page
      history.pushState({}, nextPath)
    }))
  }

  render () {
    return (
      <div className="text-left">
        <form className="explore pure-form pure-form-aligned">
          <fieldset>
            <div className="pure-control-group">
              <label htmlFor="email">
                <span className="text-danger">*</span><FormattedMessage {...messages.email} />
              </label>
              <span>{this.props.location.state.email}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="password">
                <span className="text-danger">*</span><FormattedMessage {...messages.password} />
              </label>
              {this.props.location.state.password}
            </div>
            <div className="pure-control-group">
              <label htmlFor="firstName">
                <span className="text-danger">*</span><FormattedMessage {...messages.firstName} />
              </label>
              <span>{this.props.location.state.firstName}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="lastName">
                <span className="text-danger">*</span><FormattedMessage {...messages.lastName} />
              </label>
              <span>{this.props.location.state.lastName}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="countryId">
                <span className="text-danger">*</span><FormattedMessage {...messages.category} />
              </label>
              {this.props.location.state.categoryList.map( (category) => {
                if (category.id === parseInt(this.props.location.state.categoryId)) {
                  return (<span>{category.name}</span>)
                }
              })}
              <span>{this.props.location.state.category}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="countryId">
                <span className="text-danger">*</span><FormattedMessage {...messages.country} />
              </label>
              {this.props.location.state.countryList.map( (country) => {
                if (country.id === parseInt(this.props.location.state.countryId)) {
                  return (<span>{country.name}</span>)
                }
              })}
              <span>{this.props.location.state.country}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="photo">
                <FormattedMessage {...messages.photo} />
              </label>
              <span> { (() => {
                if (this.props.location.state.photo) {
                  return (<img src= { this.props.location.state.photo } style= { { height: '200px' } } />)
                }
              } )() }
              </span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="universityName">
                <span className="text-danger">*</span><FormattedMessage {...messages.universityName} />
              </label>
              <span>{this.props.location.state.universityName}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="universityAddress">
                <FormattedMessage {...messages.universityAddress} />
              </label>
            </div>
            <div className="pure-control-group">
              <label htmlFor="resume">
                <span className="text-danger">*</span><FormattedMessage {...messages.resume} />
              </label>
              <span>{this.props.location.state.resume}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="role">
                <FormattedMessage {...messages.role} />
              </label>
              {this.props.location.state.roleTags.map( (role) => {
                return (
                    <div className="ReactTags__selected">
                      <span className="ReactTags__tag">
                        <span>{role.text}</span>
                      </span>
                    </div>
                )
              })}
            </div>
            <div className="pure-control-group">
              <label htmlFor="language">
                <FormattedMessage {...messages.language} />
              </label>
              {this.props.location.state.languageTags.map( (language) => {
                return (
                    <div className="ReactTags__selected">
                      <span className="ReactTags__tag">
                        <span>{language.text}</span>
                      </span>
                    </div>
                )
              })}
            </div>
            <div className="pure-control-group">
              <label htmlFor="location">
                <FormattedMessage {...messages.location} />
              </label>
              <span>{this.props.location.state.location}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="video">
                <span className="text-danger">*</span><FormattedMessage {...messages.video} />
              </label>
              <span>{this.props.location.state.video}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="facebook">Facebook</label>
              <span>{this.props.location.state.facebook}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="linkedin">LinkedIn</label>
              <span>{this.props.location.state.linkedin}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="github">GitHub</label>
              <span>{this.props.location.state.github}</span>
            </div>
            <div className="pure-control-group">
              <label htmlFor="skypeId">
                <span className="text-danger">*</span>
                SkypeId
              </label>
              <span>{this.props.location.state.skypeId}</span>
            </div>

          <ButtonToolbar>
            <button bsStyle="primary" onClick={::this.backSubmit}>
              <FormattedMessage {...messages.back}/>
            </button>
            <button bsStyle="primary" onClick={::this.handleSubmit}>
              <FormattedMessage {...messages.register} />
            </button>
          </ButtonToolbar>
          </fieldset>
        </form>
      </div>
    )
  }
}


RegisterStudentConfirm.propTypes = {
  location: React.PropTypes.object,
  history: React.PropTypes.object
}
