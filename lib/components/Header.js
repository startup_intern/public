import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { defineMessages, FormattedMessage } from 'react-intl'
import Modal from 'react-modal'
import * as actions from '../actions/user'
import apiClient from '../utils/api'
// import { Link } from 'react-router'

const messages = defineMessages({
  builtWith: {
    id: 'header.builtWith',
    description: 'This is header',
    defaultMessage: 'StartupIntern.com'
  },
  login: {
    id: 'header.login',
    description: 'login',
    defaultMessage: 'Login'
  },
  logout: {
    id: 'header.logout',
    description: 'logout',
    defaultMessage: 'Logout'
  },
  register: {
    id: 'header.register',
    description: 'register',
    defaultMessage: 'Register'
  },
  myPage: {
    id: 'header.myPage',
    description: 'myPage',
    defaultMessage: 'MyPage'
  },
  startup: {
    id: 'header.startup',
    description: 'startup button',
    defaultMessage: 'Register as startup company'
  },
  student: {
    id: 'header.student',
    description: 'student button',
    defaultMessage: 'Register as student or trainer'
  },
  company: {
    id: 'header.company',
    description: 'company button',
    defaultMessage: 'Register as company'
  },
  invalidError: {
    id: 'header.invalidError',
    description: 'invalid error',
    defaultMessage: 'Invalid email or password'
  }
})

const customStyles = {
  content : {
    top                   : '25%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

export default class Header extends React.Component {

  static propTypes = {
    location: PropTypes.object
  };

  static contextTypes = {
    store: PropTypes.any,
    history: PropTypes.object.isRequired
  };

  constructor (props) {
    super(props)
    this.state = {
      loginModalIsOpen: props.loginModalIsOpen,
      registerModalIsOpen: props.registerModalIsOpen,
      registerSocialModalIsOpen: props.registerSocialModalIsOpen,
      registerNextPath: props.registerNextPath,
      email: null,
      password: null,
      errors: {
        login: null
      },
      errorNo: null,
      firstName: null,
      lastName: null,
      authId: null,
    }
    this.openLoginModal = this.openLoginModal.bind(this)
    this.closeLoginModal = this.closeLoginModal.bind(this)
    this.openRegisterModal = this.openRegisterModal.bind(this)
    this.closeRegisterModal = this.closeRegisterModal.bind(this)
    this.openRegisterSocialModal = this.openRegisterSocialModal.bind(this)
    this.closeRegisterSocialModal = this.closeRegisterSocialModal.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.studentSubmit = this.studentSubmit.bind(this)
    this.startupSubmit = this.startupSubmit.bind(this)
    this.companySubmit = this.companySubmit.bind(this)
    this.registerNextSubmit = this.registerNextSubmit.bind(this)
    this.statusChangeCallback = this.statusChangeCallback.bind(this)
    this.checkLoginState = this.checkLoginState.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.facebookLogin　= this.facebookLogin.bind(this)
    this.loginCheck = this.loginCheck.bind(this)
  }

  openLoginModal () {
    this.setState({ loginModalIsOpen: true })
  }

  closeLoginModal () {
    this.setState({ loginModalIsOpen: false })
  }

  openRegisterModal () {
    this.setState({ registerModalIsOpen: true })
  }

  closeRegisterModal () {
    this.setState({ registerModalIsOpen: false })
  }

  openRegisterSocialModal () {
    this.setState({ registerSocialModalIsOpen: true })
  }

  closeRegisterSocialModal () {
    this.setState({ registerSocialModalIsOpen: false })
  }

  handleInputChange (evt) {
    this.setState({
      [evt.target.name]: evt.target.value
    })
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const { history, store } = this.context
    let data = {}

    apiClient(localStorage.getItem('Authorization')).post('/user/login/', {
      email: this.state.email,
      password: this.state.password
    })
        .then((response) => {
          console.log(response.data.data)
          if (!response.data.data.error_no) {
            localStorage.setItem('Authorization', response.data.data.token)
            data = response.data.data
            this.setState({ loginModalIsOpen: false })
            store.dispatch(actions.login(this.state, history, data))
          } else {
            if (Number(response.data.data.error_no) === 401) {
              this.state.errors.login = ( <FormattedMessage {...messages.invalidError} /> )
            }
            this.setState({
              user: {
                errorNo: response.data.data.error_no
              }
            })
          }
        } )
        .catch((response) => {
          console.log(response)
        } )

  }

  studentSubmit () {
    this.setState({
      registerModalIsOpen: false,
      registerSocialModalIsOpen: true,
      registerNextPath: '/register/student'
    })
  }

  startupSubmit () {
    this.setState({
      registerModalIsOpen: false,
      registerSocialModalIsOpen: true,
      registerNextPath: '/register/startup'
    })
  }

  companySubmit () {
    this.setState({
      registerModalIsOpen: false,
      registerSocialModalIsOpen: true,
      registerNextPath: '/register/company'
    })
  }

  registerNextSubmit (evt) {
    evt.preventDefault()
    this.setState({
      registerModalIsOpen: false,
      registerSocialModalIsOpen: false,
    })
    const { history } = this.context
    let nextPath = this.state.registerNextPath
    history.pushState({}, nextPath)
  }

  loginCheck () {
    const { history, store  } = this.context
    let data = {}
    apiClient(localStorage.getItem('Authorization')).get('/user/check')
        .then((response) => {
          console.log(response)
          if (!response.data.data.error_no) {
            localStorage.setItem('Authorization', response.data.data.token)
            data = response.data.data
            store.dispatch(actions.login(this.state, history, data))
            return true
          } else {
            history.pushState({}, '/logout')
          }
        } )
        .catch((response) => {
          console.log(response)
          history.pushState({}, '/logout')
          return false
        } )
  }

  componentWillMount () {
    const { store } = this.context
    let state = store.getState()
    console.log(state)
    if (state.router.location.pathname !== '/logout') {
      this.loginCheck()
    }
  }

  componentDidMount () {

    window.fbAsyncInit = function () {
      FB.init({
        appId      : '522010701325509',
        cookie     : true,  // enable cookies to allow the server to access
        // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.1' // use version 2.1
      })

    }.bind(this);

    // Load the SDK asynchronously
    ( function (d, s, id) {
      let js, fjs = d.getElementsByTagName(s)[0]
      if (d.getElementById(id)) {
        return
      }
      js = d.createElement(s)
      js.id = id
      js.src = '//connect.facebook.net/en_US/sdk.js'
      fjs.parentNode.insertBefore(js, fjs)
    }(document, 'script', 'facebook-jssdk'))

  }


  facebookLogin (token) {
    const { history, store  } = this.context
    let data = {}
    console.log('Fetching your information.... ')
    FB.api ('/me', {
      fields: [
        'id',
        'first_name',
        'last_name',
        'email',
        'picture.type(large)',
      ] }, function (facebookResponse) {
        console.log('Successful login for: ' + facebookResponse.name)
        apiClient().post('/user/facebook/login', {
          facebookToken: token,
          facebookId: facebookResponse.id,
          email: facebookResponse.email
        })
          .then((response) => {
            if (response.data.error_no === 500) {
              this.setState({
                loginModalIsOpen: false,
                registerModalIsOpen: false,
                registerSocialModalIsOpen: false
              })
              store.dispatch(actions.addFaceBookData(
                  token,
                  facebookResponse
              ))
              history.pushState({}, this.state.registerNextPath)
            } else if (response.data.data.token) {
              localStorage.setItem('Authorization', response.data.data.token)
              data = response.data.data
              this.setState({ loginModalIsOpen: false })
              store.dispatch(actions.login(this.state, history, data))
            }
          } )
          .catch((response) => {
            console.log(response)
          } )
      }.bind(this))
  }

  // This is called with the results from from FB.getLoginStatus().
  statusChangeCallback (response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      this.facebookLogin(response.authResponse.accessToken)
    } else if (response.status === 'not_authorized') {
      console.log('Please log into this app.')
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
          'into this app.'
    } else {
      console.log('Please log into Facebook.')
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
          'into Facebook.'
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  checkLoginState () {
    FB.getLoginStatus (function (response) {
      this.statusChangeCallback (response)
    }.bind(this))
  }

  handleClick () {
    FB.login(this.checkLoginState())
  }

  render () {
    if (!localStorage.getItem('Authorization')) {
      return (
          <div className="header">
            <div className="pure-g">
              <div className="pure-u-1 u-sm-1-2">
                <FormattedMessage {...messages.builtWith} />
                <a onClick={this.openLoginModal}>
                  <FormattedMessage {...messages.login} />
                </a>
                <Modal
                    isOpen={this.state.loginModalIsOpen}
                    closeTimeoutMS={150}
                    onRequestClose={this.closeLoginModal}
                    style={customStyles} >
                  <div className="header">
                    <h1>Login</h1>
                  </div>
                  <div className="content">
                    <form
                        className="Modal__Bootstrap modal-dialog"
                        onSubmit={::this.handleSubmit}
                        onChange={::this.handleInputChange}>
                      <fieldset>
                        <div className="pure-control-group">
                          <span className="text-danger">{this.state.errors.login}</span>
                        </div>
                        <div className="pure-control-group">
                          <label htmlFor="email">Email</label>
                          <input type="email" name="email" required="required" defaultValue="" />
                        </div>
                        <div className="pure-control-group">
                          <label htmlFor="password">Password</label>
                          <input type="password" required="required"
                                 name="password"
                                 defaultValue="" />
                        </div>
                        <button type="submit"
                                className="pure-button pure-button-primary"
                        >Login</button>
                      </fieldset>
                    </form>
                    <a onClick={this.handleClick} className="btn btn-block btn-social btn-facebook">
                      <span className="fa fa-facebook"></span>
                      Login in with Facebook
                    </a>
                  </div>
                </Modal>
                <a onClick={this.openRegisterModal}>
                  <FormattedMessage {...messages.register} />
                </a>
                <Modal
                    isOpen={this.state.registerModalIsOpen}
                    closeTimeoutMS={150}
                    onRequestClose={this.closeRegisterModal}
                    style={customStyles} >
                  <div className="header">
                    <h1>Register</h1>
                  </div>
                  <div className="content Modal__Bootstrap modal-dialog">

                    <button type="submit"
                            onClick={this.studentSubmit}
                            className="btn btn-bloc btn-primary"
                    >
                      <FormattedMessage {...messages.student} />
                    </button>
                    <button type="submit"
                            onClick={::this.startupSubmit}
                            className="btn btn-bloc btn-primary"
                    >
                      <FormattedMessage {...messages.startup} />
                    </button>
                    <button type="submit"
                            onClick={::this.companySubmit}
                            className="btn btn-bloc btn-primary"
                    >
                      <FormattedMessage {...messages.company} />
                    </button>
                  </div>
                </Modal>
                <Modal
                    isOpen={this.state.registerSocialModalIsOpen}
                    closeTimeoutMS={150}
                    onRequestClose={this.closeRegisterSocialModal}
                    style={customStyles} >
                  <div className="header">
                    <h1>Register</h1>
                  </div>
                  <div className="content">
                    <a className="btn btn-block btn-primary"
                       onClick={this.registerNextSubmit}>
                      Register in email and password
                    </a>
                    <a onClick={this.handleClick}className="btn btn-block btn-social btn-facebook">
                          <span className="fa fa-facebook">
                          </span>
                      Register in with Facebook
                    </a>
                  </div>
                </Modal>
              </div>
            </div>
          </div>
      )
    } else {
      return (
          <div className="header">
            <div className="pure-g">
              <div className="pure-u-1 u-sm-1-2">
                <FormattedMessage {...messages.builtWith} />
                <a href="/logout">
                  <FormattedMessage {...messages.logout} />
                </a>
                <a to="/myPage">
                  <FormattedMessage {...messages.myPage} />
                </a>
              </div>
            </div>
          </div>
      )
    }
  }
}

Header.propTypes = {
  loginModalIsOpen: React.PropTypes.bool,
  registerSocialModalIsOpen: React.PropTypes.bool,
  registerModalIsOpen: React.PropTypes.bool,
  registerNextPath: React.PropTypes.string,
  errors: React.PropTypes.object,
  errorNo: React.PropTypes.number,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  authId: React.PropTypes.string
}

Header.defaultProps = {
  loginModalIsOpen: false,
  registerModalIsOpen: false,
  registerSocialModalIsOpen: false,
  registerNextPath: ''
}
function mapStateToProps () {
  return {
    loginModalIsOpen: false,
    registerModalIsOpen: false,
    registerSocialModalIsOpen: false,
    token: null
  }
}

export default connect(
    mapStateToProps
)(Header)
