import * as constants from '../constants'
import createReducer from '../utils/create-reducer'

const initialState = {
  token: null,
  locale: 'en',
  user: {
    // TODO: have a checkbox to update the state
    // e.g.: on the login page and/or menu
    // permissions: ['manage_account']
    permissions: [],
    email: null,
    firstName: null,
    lastName: null,
    facebookId: null,
    facebookToken: null,
    photo: null,
    authId: null
  },
  error: null,
  errorNo: null
}

const actionHandlers = {
  [constants.LOGGED_IN]: (state, action) => ({ ...state, ...{
    token: action.payload,
    user: {
      email: action.user.email,
      firstName: action.user.firstName,
      lastName: action.user.lastName,
      photo: action.user.photo,
      authId: action.user.authId
    }
  } } ),
  [constants.LOGGED_IN_ERROR]: (state, action) => ({ ...state, ...{ errorNo: action.payload.errorNo } }),
  [constants.ADD_FACEBOOK_DATA]: (state, action) => ({ ...state, ...{
    user: {
      email: action.user.email,
      firstName: action.user.firstName,
      lastName: action.user.lastName,
      photo: action.user.photo,
      facebookId: action.user.facebookId,
      facebookToken: action.user.facebookToken
    }
  } }),
  [constants.LOG_OUT]: () => ({
    token: null,
    user: {
      email: null,
      firstName: null,
      lastName: null,
      photo: null,
      authId: null
    }
  }),
  [constants.LOCALE_SWITCHED]: (_, action) => ({ locale: action.payload }),

  // TODO: this handle only API error responses.
  // We should also handle all other kind of application errors,
  // report them and show some kind of helpful message to the user.
  [constants.SHOW_ERROR]: (state, action) => {
    const { payload, source } = action
    return Object.assign({}, state, {
      // TODO: ideally we want to map API error response codes
      // with some user-friendly messages.
      error: {
        source,
        message: payload.message,
        statusCode: payload.statusCode || payload.code,
        body: payload.body || (payload instanceof Error ?
          (payload.toString() + '\n' + payload.stack) : payload)
      }
    })
  },
  [constants.HIDE_ERROR]: state => ({ ...state, ...{ error: null } }),
}

export default createReducer(initialState, actionHandlers)
