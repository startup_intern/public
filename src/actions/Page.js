/**
 * Created by masahirohanawa on 6/21/16.
 */

export function addPlace(name, dueTo) {
    return {
        type: 'ADD_PLACE',
        todo: { name, dueTo },
    };
}
