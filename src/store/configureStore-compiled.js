'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = configureStore;

var _routes = require('../routes');

var _routes2 = _interopRequireDefault(_routes);

var _reduxRouter = require('redux-router');

var _reduxDevtools = require('redux-devtools');

var _createBrowserHistory = require('history/lib/createBrowserHistory');

var _createBrowserHistory2 = _interopRequireDefault(_createBrowserHistory);

var _redux = require('redux');

var _reducers = require('../reducers');

var _reducers2 = _interopRequireDefault(_reducers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var finalCrateStore = (0, _redux.compose)((0, _reduxRouter.reduxReactRouter)({ routes: _routes2.default, createHistory: _createBrowserHistory2.default }), (0, _reduxDevtools.devTools)())(_redux.createStore);

function configureStore(initialState) {
    var store = finalCrateStore(_reducers2.default, initialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', function () {
            var nextReducer = require('../reducers');
            store.replaceReducer(nextReducer);
        });
    }

    return store;
}

//# sourceMappingURL=store-compiled.js.map