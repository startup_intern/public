/**
 * Created by masahirohanawa on 6/21/16.
 */

export const ADD_TODO = 'ADD_TODO';
export const DEL_TODO = 'DEL_TODO';
export const CHANGE_DID_FLAG = 'CHANGE_DID_FLAG';

export const SAVE_LIST_AJAX_REQUEST = 'SAVE_LIUST_AJAX_REQUEST';
export const SAVE_LIST_AJAX_RESULT = 'SAVE_LIST_AJAX_RESULT';
