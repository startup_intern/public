// router.js
'use strict';

var _router;

module.exports = {
    get: function() {
        return _router;
    },

    set: function(router) {
        _router = router;
    }
};