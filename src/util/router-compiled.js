// router.js
'use strict';

var _router;

module.exports = {
    get: function get() {
        return _router;
    },

    set: function set(router) {
        _router = router;
    }
};

//# sourceMappingURL=router-compiled.js.map