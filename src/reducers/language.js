import { REGISTER_CONFIRM } from '../constants';

const initialState = {
    email: '',
};

export default function registerConfirm(state = initialState, action) {

    switch (action.type) {
        case REGISTER_CONFIRM:
            console.log(action.type);
            return Object.assign({}, state, {
                email: action.email,
            });
        default:
            return state;
    }
}
