/**
 * Created by masahirohanawa on 6/21/16.
 */
import React from 'react';

const initialState = {
    markers: {
        markers: [{
            position: {
                lat: 25.0112183,
                lng: 121.52067570000001,
            },
            key: 'AIzaSyAxMQWlE5KtExxkac-ZaGNxX9oRMHwpYdE',
            defaultAnimation: 2,
        }],
    },
};

export default function map(state = initialState, action) {
    let markers = this.state;
    switch (action.type) {
    case 'ADD_PLACE':
        markers = React.update(markers, {
            $push: [
                {
                    position: event.latLng,
                    defaultAnimation: 2,
                    key: Date.now(), // Add a key property for: http://fb.me/react-warning-keys
                },
            ],
        });
        this.setState({ markers });

        if (markers.length === 3) {
            this.props.toast(
                'Right click on the marker to remove it',
                'Also check the code!'
            );
        }
        return state;
        break;
    default:
        return state;
    }
}
