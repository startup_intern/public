import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Parent from './components/default/Parent';
import ChildA from './components/default/ChildA.jsx';
import ChildB from './components/default/ChildB.jsx';
import GrandChildA from './components/default/GrandChildA.jsx';
import GrandChildB from './components/default/GrandChildB.jsx';
import NotFound from './components/default/NotFound.jsx';


export default (

    <Route  path="/" component={Parent} >
        <IndexRoute component={ChildA} />
        <Route path="child_a" component={ChildA} />
        <Route path="/child_b/:id" component={ChildB} >
            <IndexRoute component={GrandChildA} />
            <Route path="gchild_a" component={GrandChildA} />
            <Route path="gchild_b" component={GrandChildB} />
        </Route>
        <Route path="*" component={NotFound} />
    </Route>
);

