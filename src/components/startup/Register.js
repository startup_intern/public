import React from 'react';
import { Link, browserHistory } from 'react-router';
import { createStore, combineReducers } from 'redux';
import { addLocaleData, FormattedMessage } from 'react-intl';
import { IntlProvider, intlReducer } from 'react-intl-redux';
import LocaleMenu from './LocaleMenu';

export default function App({ children }) {

   return (
        <div>
            <header>
                {' '}
                <Link to="/">Home</Link>
                {' '}
                <Link to="/login">Login</Link>
                <button onClick={() => changeLanguage('ja')}>japan</button>
                <Link to="/intern/register">student_register</Link>
                <Link to="/startup/register">startup_register</Link>
            </header>
            <div>
                <button onClick={() => browserHistory.push('/foo')}>Go to /foo</button>
            </div>
            <div style={{ marginTop: '1.5em' }}>{children}</div>
            <FormattedMessage id="app.greeting" defaultMessage="hello!" />
            <footer>
                {' '}
                <Link to="/">Home</Link>
                {' '}
                <Link to="/login">Login</Link>
            </footer>
        </div>
    )
};

