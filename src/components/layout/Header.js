import React, { PropTypes, Component } from 'react';
import Link from 'react-router';

class Header extends Component {
  static get childContextTypes() {
    return { muiTheme: React.PropTypes.object };
  }

  getChildContext(){
    return {  };
  }

  render() {
    return (
      <header className="header">
          test
      </header>
    );
  }
}

Header.propTypes = {
};

export default Header;
