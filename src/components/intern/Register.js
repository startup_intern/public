import React from 'react';
import { Link, browserHistory } from 'react-router';
import { createStore, combineReducers } from 'redux';
import { addLocaleData, FormattedMessage } from 'react-intl';
import { IntlProvider, intlReducer } from 'react-intl-redux';

export default function InternRegister({ children }) {

   return (
        <div>
            <header>
                {' '}
                <Link to="/">Home</Link>
                {' '}
                <Link to="/login">Login</Link>
                <button onClick={() => changeLanguage('ja')}>japan</button>
                <Link to="/intern/register">student_register</Link>
                <Link to="/startup/register">startup_register</Link>
            </header>
            <article>
                <form id="student_confirm">
                    first name<input type="text" name="first_name" value="" />
                    last name<input type="text" name="last_name" value="" />
                    country<input type="text" name="country" value="" />
                    univercity<input type="text" name="univercity" value="" />
                    univercity address<input type="text" name="univercity_address" value="" />
                    mini-resume<textarea name="resume"></textarea>
                    Role<select name="role">
                    <option value="php">php</option>
                    <option value="html">html</option>
                    <option value="css">css</option>
                    <option value="ruby">ruby</option>
                    </select>
                    Language<select name="language">
                        <option value="english">english</option>
                        <option value="japanese">japanese</option>
                        <option value="korean">korean</option>
                        <option value="german">german</option>
                    </select>
                    Location<input type="text" name="location" value="" />
                    introduce video<input type="text" name="video" value="" />
                    facebook<input type="text" name="facebook" value="" />
                    linkedin<input type="text" name="linkedin" value="" />
                    github<input type="text" name="github" value="" />
                    skypeid<input type="text" name="skypeid" value="" />
                    <button className="back" onClick={this.history.goBack}>{this.props.children}</button>
                </form>
            </article>
            <div style={{ marginTop: '1.5em' }}>{children}</div>
            <FormattedMessage id="app.greeting" defaultMessage="hello!" />
            <footer>
                {' '}
                <Link to="/">Home</Link>
                {' '}
                <Link to="/login">Login</Link>
            </footer>
        </div>
    )
};

